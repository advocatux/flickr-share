/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.h"

#include <QDebug>
#include <QSettings>
#include <QStandardPaths>

using namespace Uploader;

namespace Uploader {

const QString keyPublic = QStringLiteral("isPublic");
const QString keyFriends = QStringLiteral("isFriends");
const QString keyFamily = QStringLiteral("isFamily");
const QString keyHidden = QStringLiteral("isHidden");

class SettingsPrivate
{
    Q_DECLARE_PUBLIC(Settings)

public:
    SettingsPrivate(Settings *q);
    ~SettingsPrivate();


private:
    QSettings m_settings;
    mutable Settings *q_ptr;
};

} // namespace

SettingsPrivate::SettingsPrivate(Settings *q):
    q_ptr(q)
{
}

SettingsPrivate::~SettingsPrivate()
{
}

Settings::Settings(QObject *parent):
    QObject(parent),
    d_ptr(new SettingsPrivate(this))
{
}

Settings::~Settings()
{
    delete d_ptr;
}

void Settings::setPublic(bool value)
{
    Q_D(Settings);
    d->m_settings.setValue(keyPublic, value);
    Q_EMIT isPublicChanged();
}

bool Settings::isPublic() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyPublic, false).toBool();
}

void Settings::setFriends(bool value)
{
    Q_D(Settings);
    d->m_settings.setValue(keyFriends, value);
    Q_EMIT isFriendsChanged();
}

bool Settings::isFriends() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyFriends, false).toBool();
}

void Settings::setFamily(bool value)
{
    Q_D(Settings);
    d->m_settings.setValue(keyFamily, value);
    Q_EMIT isFamilyChanged();
}

bool Settings::isFamily() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyFamily, false).toBool();
}

void Settings::setHidden(bool value)
{
    Q_D(Settings);
    d->m_settings.setValue(keyHidden, value);
    Q_EMIT isHiddenChanged();
}

bool Settings::isHidden() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyHidden, true).toBool();
}
