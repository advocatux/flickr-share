include(../common-config.pri)

TEMPLATE = app
TARGET = app

QT += \
    concurrent \
    qml \
    quick \
    xml

CONFIG += \
    c++11 \
    link_pkgconfig

PKGCONFIG += \
    exiv2

SOURCES += \
    main.cpp \
    metadata.cpp \
    settings.cpp \
    upload_model.cpp \
    uploader.cpp

HEADERS += \
    metadata.h \
    settings.h \
    upload_model.h \
    uploader.h

RESOURCES += \
    app.qrc \
    $${TOP_SRC_DIR}/data/icons/icons.qrc

DEFINES += \
    APPLICATION_NAME=\\\"$${APPLICATION_NAME}\\\"

target.path = $${INSTALL_PREFIX}/bin
INSTALLS+=target
