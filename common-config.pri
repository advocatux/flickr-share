PROJECT_VERSION = 0.5

APPLICATION_NAME = "it.mardy.uploader"
# Use this function just to get the title extracted for translation
defineReplace(tr) { return($$1) }
APPLICATION_TITLE = $$tr("Flickr uploader")

CONFIG(qtc) {
    INSTALL_PREFIX = /
} else {
    INSTALL_PREFIX = $${TOP_BUILD_DIR}/click
}
CLICK_ARCH = $$system("dpkg-architecture -qDEB_HOST_ARCH")
